<?php
class Example_Controller {
    public $template = 'example';
    public function main(array $getVars) {
        $exampleModel = new Example_Model;
        $user = $exampleModel->get_user($getVars['uid']);
        $view = new View_Model($this->template);
        $view->assign('username' , $user['username']);
        $view->assign('email' , $user['email']);
		
		$header = new View_Model('header');
		$footer = new View_Model('footer');
		$master = new View_Model('master');
		$master->assign('header', $header->render(FALSE));
		$master->assign('body' , $view->render(FALSE));
		$master->assign('footer' , $footer->render(FALSE));
		$master->render();
    }
}
?>