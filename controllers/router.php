<?php
function __autoload($className) {
    list($suffix, $filename) = preg_split('/_/', strrev($className), 2);
    $filename = strrev($filename);
    $suffix = strrev($suffix);

    switch (strtolower($suffix)) {    
        case 'model':
            $folder = '/models/';
			break;
        case 'library':
            $folder = '/libraries/';
			break;
        case 'driver':
            $folder = '/libraries/drivers/';
			break;
    }

    $file = SERVER_ROOT . $folder . strtolower($filename) . '.php';

    if (file_exists($file)) {
        include_once($file);        
    } else {
        die("File '$filename' containing class '$className' not found in '$folder'.");    
    }
}

$request = $_SERVER['QUERY_STRING'];
$parsed = explode('&' , $request);

$page = array_shift($parsed);

$getVars = array();
foreach ($parsed as $argument) {
    list($variable , $value) = split('=' , $argument);
    $getVars[$variable] = urldecode($value);
}

$target = SERVER_ROOT . '/controllers/' . $page . '.php';
if (file_exists($target)) {
    include_once($target);
    $class = ucfirst($page) . '_Controller';
    if (class_exists($class)) {
        $controller = new $class;
    } else {
        die('class does not exist!');
    }
} else {
    die('page does not exist!');
}

$controller->main($getVars);
?>