<?php
class MysqlImproved_Driver extends Database_Library {
    private $connection;
    private $query;
    private $result;

    public function connect($database) {
        $host = 'localhost';
        $user = 'root';
        $password = 'P@ssw0rd1!';

        $port = NULL;
        $socket = NULL;
    
        $this->connection = new mysqli (
            $host , $user , $password , $database , $port , $socket
        );
    }

    public function disconnect() {
		$this->connection->close();
        return TRUE;
	}
	
    public function prepare($query) {
		$this->query = $query;    
        return TRUE;
	}
	
    public function query() {
		if (isset($this->query)) {
            $this->result = $this->connection->query($this->query);
            return TRUE;
        }
        return FALSE;  
	}
	
    public function fetch($type = 'object') {
		if (isset($this->result)) {
            switch ($type) {
                case 'array':
                    $row = $this->result->fetch_array();
					break;
                case 'object':
				//fall through...
                default:
                    $row = $this->result->fetch_object();    
					break;
            }
            return $row;
        }
        return FALSE;
	}
	
	public function escape($data) {
		return $this->connection->real_escape_string($data);
	}
}
?>