<?php
abstract class Database_Library {
    abstract protected function connect($database);
    abstract protected function disconnect();
    abstract protected function prepare($query);
    abstract protected function query();
    abstract protected function fetch($type = 'object');
}
?>