<?php
class Example_Model {
	private $db;
    
    public function __construct() {
        $this->db = new MysqlImproved_Driver;
    }

    public function get_user($uid) {        
        $this->db->connect("hr");
		$uid = $this->db->escape($uid);
        $this->db->prepare("
			SELECT `username`, `email`
			FROM `users` 
			WHERE `userid` = '$uid' 
			LIMIT 1;
		");
        $this->db->query();
        $user = $this->db->fetch('array');
        return $user;
    }
}
?>